### Mapping using highmaps

These explorations were part of a INFO VIZ project.

This is a brief exploration of mapping using highmaps and some random data with categorical and continuous (random) associated variables.

Highmaps provides nice built in interactions (can zoom/ pan around, series are clickable etc.).