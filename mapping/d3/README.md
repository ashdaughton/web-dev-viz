### Mapping using D3

These explorations were part of a INFO VIZ project.

This is a brief exploration of mapping using D3 and some random data with categorical and continuous (random) associated variables. Data are colored by the "cat_var_1" variable. Tool tips show the number of records with the associated feature.

Key labels aren't showing up and there's a random '0' in the left corner. Perhaps play with more interactivity at a later point.