## Sample Web Development Visualizations

This is a compliation of different javascript I've used for web development. It is a playground for trying out new, simple tools.

Visualizations currently included here are:

* Interactive maps

## Built with 
* [d3](https://d3js.org/)
* [Highcarts](https://www.highcharts.com/) 

